from kubernetes import client, config, utils
import os
import random
import base64


class K8sClient:

    def __init__(self):
        self.token = os.environ.get('K8S_TOKEN')
        self.api = os.environ.get('K8S_API')
        self.configuration = client.Configuration()

        # Doe dit niet in productie, do as I sat not as I do, het is al laat!
        self.configuration.verify_ssl = False
        ####
        self.configuration.api_key = {"authorization": "Bearer " + self.token}
        self.configuration.host = self.api
        self.apiClient = client.ApiClient(self.configuration)

    def create_new_namespace(self, name):
        backup_list = ["henk", "enterprise", "k8s", "love", "wollah", "gun", "dikke", "bmw", "bietcoin", "halloween",
                       "agro"]

        random.shuffle(backup_list)
        random_words = backup_list[:3]

        postfix = "-".join(random_words)
        namespace = "ns-" + name.lower() + "-" + postfix

        v1_core = client.CoreV1Api(self.apiClient)
        v1_rbac = client.RbacAuthorizationV1Api(self.apiClient)

        # Create namespace
        ns = client.V1Namespace()
        ns.metadata = client.V1ObjectMeta(name=namespace)
        v1_core.create_namespace(ns)

        # Create service account
        sa = client.V1ServiceAccount()
        sa.metadata = client.V1ObjectMeta(name="sa-%s-admin" % namespace, namespace=namespace)
        v1_core.create_namespaced_service_account(namespace=namespace, body=sa)

        # Create Role
        role = client.V1Role()
        role.metadata = client.V1ObjectMeta(name='%s-admin-role' % namespace, namespace=namespace)
        role.rules = [client.V1PolicyRule(api_groups=["*"], resources=["*"], verbs=["*"])]
        v1_rbac.create_namespaced_role(namespace=namespace, body=role)

        # Create Rolebinding
        role_binding = client.V1RoleBinding(
            role_ref=client.V1RoleRef(api_group="rbac.authorization.k8s.io", kind="Role",
                                      name='%s-admin-role' % namespace))
        role_binding.metadata = client.V1ObjectMeta(name='%s-admin-rolebinding' % namespace, namespace=namespace)
        role_binding.subjects = [
            client.V1Subject(namespace=namespace, kind="ServiceAccount", name="sa-%s-admin" % namespace)]
        v1_rbac.create_namespaced_role_binding(namespace=namespace, body=role_binding)

        return namespace

    def get_kubectl_data(self, namespace):
        cluster_name = "space_workshop_cluster"
        service_account_name = "sa-%s-admin" % namespace
        server = self.api
        v1_core = client.CoreV1Api(self.apiClient)
        service_account = v1_core.read_namespaced_service_account(name=service_account_name,
                                                                  namespace=namespace)  # type: client.V1ServiceAccount
        secret_name = service_account.secrets[0].name
        secret = v1_core.read_namespaced_secret(namespace=namespace, name=secret_name)  # type: client.V1Secret
        ca_cert = secret.data['ca.crt']
        token = base64.b64decode(secret.data['token']).decode('utf-8')

        return {
            'cluster_name': cluster_name,
            'service_account': service_account.metadata.name,
            'server': self.api,
            'token': token,
            'ca_cert': ca_cert,
            'ns': namespace
        }
