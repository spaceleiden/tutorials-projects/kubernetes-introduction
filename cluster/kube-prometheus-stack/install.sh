#!/bin/bash

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install prometheus-stack -f ./values.yaml -n monitoring prometheus-community/kube-prometheus-stack
