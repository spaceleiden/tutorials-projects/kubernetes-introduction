#!/bin/bash


helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm install prometheus -n monitoring -f values.yaml prometheus-community/prometheus
